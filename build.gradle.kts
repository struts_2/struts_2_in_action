plugins {
    java
    war
}

repositories {
    jcenter()
}

dependencies {
    providedCompile("javax.servlet:servlet-api:2.5")
    implementation("org.apache.struts:struts2-core:2.5.22")
    implementation("ch.qos.logback:logback-classic:1.2.3")
    implementation("org.slf4j:slf4j-api:1.2.3")
    testImplementation("org.junit.jupiter:junit-jupiter-api:5.6.2")
    testRuntimeOnly("org.junit.jupiter:junit-jupiter-engine:5.6.2")
}

val test by tasks.getting(Test::class) {
    useJUnitPlatform()
}

java {
    sourceCompatibility = JavaVersion.VERSION_1_8
    targetCompatibility = JavaVersion.VERSION_1_8
}

tasks.war {
    archiveFileName.set("ROOT.war")
}

tasks.compileJava {
    options.compilerArgs.add("-Xlint")
}

defaultTasks("build")